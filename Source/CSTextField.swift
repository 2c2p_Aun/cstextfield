//
//  CSTextField.swift
//  CSTextField
//
//  Created by Chatchawal Saesee on 12/1/15.
//  Copyright © 2015 2C2P. All rights reserved.
//

import UIKit
import Foundation

@objc public protocol CSTextFieldDelegate {
    optional func textFieldLeftButtonPressed(button: UIButton)
    optional func textFieldRightButtonPressed(button: UIButton)
    optional func textFieldValidation(textField: UITextField, result :Bool)
}

public class CSTextField: UIView, UITextFieldDelegate {
    
    public var delegate            :CSTextFieldDelegate!
    public var textField           = UITextField()
    public var validation          = Validation.None
    
    public enum Validation {
        case None
        case Email
        case Digits
        case CreditCard
        case Range(min :Int, max: Int)
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.setCSTextField(frame)
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func setCSTextField(frame :CGRect) {
        self.textField = UITextField(frame: CGRectMake(0, 0, frame.size.width, frame.size.height))
        self.textField.delegate = self
        self.addSubview(self.textField)
    }
    
    func setPlaceholder(text :String) {
        self.textField.placeholder = text
    }
    
    func setText(text :String) {
        self.textField.text = text
    }
    
    func leftButtonPressed(button :UIButton) {
        //print("leftButtonPressed")
        self.delegate.textFieldLeftButtonPressed!(button)
    }

    func rightButtonPressed(button :UIButton) {
        //print("rightButtonPressed")
        self.delegate.textFieldRightButtonPressed!(button)
    }
    
    //MARK: Validation methods
    func isEmail(email :String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(email)
    }
    
    func isNumber(number :String) -> Bool {
        if (Int(number) != nil) {
            return true
        }else{
            return false
        }
    }
    
    func isCreditCard(number :String) -> Bool {
        // 16 digits and begin with 4 or 5 for VISA and MASTER card only
        if number.characters.count == 16 {
            let first = number.characters.first
            if first == "4" || first == "5" {
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    
    func isRange(string :String, min :Int, max :Int) -> Bool {
        if (string.characters.count >= min && string.characters.count <= max) {
            return true
        }else{
            return false
        }
    }
    
    //MARK: UITextFieldDelegate
    public func textFieldDidEndEditing(textField: UITextField) {
        switch self.validation {
        case .None:
            self.delegate.textFieldValidation!(textField, result: true)
        case .Email:
            self.delegate.textFieldValidation!(textField, result: self.isEmail(textField.text!))
        case .Digits:
            self.delegate.textFieldValidation!(textField, result: self.isNumber(textField.text!))
        case .CreditCard:
            self.delegate.textFieldValidation!(textField, result: self.isCreditCard(textField.text!))
        case .Range(let min, let max):
            self.delegate.textFieldValidation!(textField, result: self.isRange(textField.text!, min: min, max: max))
        }
    }
    
    /** CSTextField: set line under text field */
    public func setLineColor(color :UIColor, height :CGFloat) {
        let line = UIView(frame: CGRectMake(0, bounds.height - height, bounds.size.width, height))
        line.backgroundColor = color
        self.textField.addSubview(line)
    }
    
    /** CSTextField: set leftImage required textFieldLeftButtonPressed delegate method for TouchUpInside event */
    public func setLeftImage(image :UIImage) {
        let button = UIButton(frame: CGRectMake(0, 0, bounds.size.height - 6, bounds.size.height - 6))
        button.addTarget(self, action: Selector("leftButtonPressed:"), forControlEvents: .TouchUpInside)
        button.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        button.setImage(image, forState: UIControlState.Normal)
        
        let view = UIView(frame: CGRectMake(0, 0, bounds.size.height, bounds.size.height - 6))
        view.addSubview(button)
        
        self.textField.leftViewMode = UITextFieldViewMode.Always
        self.textField.leftView = view
    }
    
    /** CSTextField: set rightImage required textFieldLeftButtonPressed delegate method for TouchUpInside event */
    public func setRightImage(image :UIImage) {
        let left = bounds.size.width - bounds.size.height
        let button = UIButton(frame: CGRectMake(left, 3, bounds.size.height - 6, bounds.size.height - 6))
        button.addTarget(self, action: Selector("rightButtonPressed:"), forControlEvents: .TouchUpInside)
        button.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        button.setImage(image, forState: UIControlState.Normal)
        
        self.textField.rightViewMode = UITextFieldViewMode.Always
        self.textField.rightView = button
    }
}











