//
//  ViewController.swift
//  CSTextField
//
//  Created by Chatchawal Saesee on 12/1/15.
//  Copyright © 2015 2C2P. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController , CSTextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let text1 = CSTextField(frame: CGRectMake(10, 100, 300, 30))
        text1.setLineColor(UIColor.blueColor(), height: 1.0)
        text1.setLeftImage(UIImage(named: "ic_email")!)
        text1.setRightImage(UIImage(named: "icon")!)
        text1.textField.placeholder = "palceholder"
        text1.textField.text = "Text 1"
        text1.textField.tag = 101
        text1.validation = CSTextField.Validation.Range(min: 1, max: 5)
        text1.delegate = self
        self.view.addSubview(text1)
        
        
        let text2 = CSTextField(frame: CGRectMake(10, 150, 300, 30))
        text2.setLineColor(UIColor.redColor(), height: 1.0)
        text2.textField.placeholder = "palceholder"
        text2.textField.text = "Text 2"
        text2.textField.tag = 102
        text2.validation = CSTextField.Validation.Range(min: 1, max: 5)
        text2.delegate = self
        self.view.addSubview(text2)
        
        let tap = UITapGestureRecognizer(target: self, action: Selector("dismissKeyboard"))
        tap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }

    // MARK: UITextFieldDelegate
    func textFieldLeftButtonPressed(button: UIButton) {
        print("textFieldLeftButtonPressed in ViewController")
    }
    
    func textFieldRightButtonPressed(button: UIButton) {
        print("textFieldLeftButtonPressed in ViewController")
    }
    
    func textFieldValidation(textField: UITextField, result: Bool) {
        if (textField.tag == 101) {
            print("TEXT101: \(result)")
        }else if (textField.tag == 102){
            print("TEXT102: \(result)")
        }
    }
}

