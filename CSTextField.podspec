Pod::Spec.new do |s|
  s.name = 'CSTextField'
  s.version = '1.0.4'
  s.license = 'MIT'
  s.summary = 'TextField Validation'
  s.homepage = 'https://bitbucket.org/2c2p_Aun/cstextfield'
  s.social_media_url = 'http://twitter.com/iamaunz'
  s.authors = { 'Aun Chatchawal' => 'chatchawal@2c2p.com' }
  s.source = { :git => 'https://2c2p_Aun@bitbucket.org/2c2p_Aun/cstextfield.git', :tag => s.version }

  s.ios.deployment_target = '8.0'
  s.frameworks = 'UIKit', 'Foundation'
  s.source_files = 'Source/*.swift'

  s.requires_arc = true
end